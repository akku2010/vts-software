import { Component, QueryList, ViewChildren } from '@angular/core';

import { Platform, IonRouterOutlet, ModalController, PopoverController, ActionSheetController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
// import { DashboardPage } from './tabs/dashboard/dashboard.page';
import { TabsService } from './tabs/tabs.service';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';
// import { AuthService } from './services/auth.service';
// import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  // set up hardware back button event.
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  rootPage: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public tabs: TabsService,
    private authenticationService: AuthenticationService,
    private router: Router,
    // private alertController: AlertController,
    // private toast: ToastController,
    private modalCtrl: ModalController,
    private popoverCtrl: PopoverController,
    private actionSheetCtrl: ActionSheetController,
    // private auth: AuthService
    // private push: Push
  ) {
    this.initializeApp();
    // Initialize BackButton Eevent.
    this.backButtonEvent();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.rootPage = DashboardPage;
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.authenticationService.authState.subscribe(state => {
        if (state) {
          this.router.navigate(['maintabs']);
        } else {
          this.router.navigate(['login']);
        }
      });

      // this.auth.authState.subscribe(state => {
      //   // debugger
      //   if (state) {
      //     this.router.navigate(['maintabs']);
      //   } else {
      //     this.router.navigate(['login']);
      //   }
      // });

      // this.pushSetup();
    });
  }

  // active hardware back button
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      // close action sheet
      try {
        const element = await this.actionSheetCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close popover
      try {
        const element = await this.popoverCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close modal
      try {
        const element = await this.modalCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);

      }

      // close side menua
      // try {
      //   const element = await this.menu.getOpen();
      //   if (element !== null) {
      //     this.menu.close();
      //     return;

      //   }

      // } catch (error) {

      // }

      this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
        debugger
        if (outlet && outlet.canGoBack()) {
          // outlet.pop();
          this.router.navigateByUrl('/maintabs')

        } else if (this.router.url === '/login') {
          if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
            // this.platform.exitApp(); // Exit from app
            navigator['app'].exitApp(); // work for ionic 4

          } else {
            navigator['app'].exitApp();
            // if (this.router.isActive('/miantabs', true)) {
              // this.alertController.create({
              //   message: 'Do you want to exit the app?',
              //   buttons: [
              //     {
              //       text: 'YES PROCCED',
              //       handler: () => {
              //         navigator['app'].exitApp();
              //       }
              //     },
              //     {
              //       text: 'Back',
              //     }
              //   ]
              // }).then(alertEl => {
              //   alertEl.present();
              // })
            // }
            // this.toast.show(
            //   `Press back again to exit App.`,
            //   '2000',
            //   'center')
            //   .subscribe(toast => {
            //     // console.log(JSON.stringify(toast));
            //   });
            

            // this.toast.create({
            //   message: 'Press back again to exit App.',
            //   duration: 2000,
            //   position: 'middle'
            // }).then((toastEl => {
            //   toastEl.present();
            // }))
            this.lastTimeBackPress = new Date().getTime();
          }
        }
      });
    });
  }


  // pushSetup() {
  //   // to check if we have permission
  //   this.push.hasPermission()
  //     .then((res: any) => {

  //       if (res.isEnabled) {
  //         console.log('We have permission to send push notifications');
  //       } else {
  //         console.log('We do not have permission to send push notifications');
  //       }

  //     });

  //   // to initialize push notifications

  //   const options: PushOptions = {
  //     android: {},
  //     ios: {
  //       alert: 'true',
  //       badge: true,
  //       sound: 'false'
  //     },
  //     windows: {},
  //     browser: {
  //       pushServiceURL: 'http://push.api.phonegap.com/v1/push'
  //     }
  //   }

  //   const pushObject: PushObject = this.push.init(options);


  //   pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));

  //   pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));

  //   pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));


  // }
}
