import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  GoogleMapsMapTypeId,
  LatLngBounds,
  LatLng,
  Spherical,
  MarkerOptions
} from '@ionic-native/google-maps/ngx';
import { AppService } from 'src/app/app.service';
import { Storage } from '@ionic/storage';
import { Platform, ToastController, LoadingController, Events } from '@ionic/angular';
import { URLs } from 'src/app/app.model';
import * as moment from 'moment';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  userdetails: any;
  @ViewChild('map', {
    static: true
  }) element: ElementRef;
  map: GoogleMap;
  paramData: any;
  target: number;
  datetimeStart: any;
  datetimeEnd: any;
  hideplayback: boolean = false;
  trackerId: any;
  trackerType: any;
  DeviceId: any;
  trackerName: any;
  data2: any;
  dataArrayCoords: any[];
  mapData: any[];
  playing: boolean;
  coordreplaydata: any;
  startPos: any[];
  speed: number;
  speedMarker: any;
  updatetimedate: any;
  allData: any = {};

  constructor(
    private route: ActivatedRoute,
    public googleMaps: GoogleMaps,
    public appService: AppService,
    public elementRef: ElementRef,
    private ionicStorage: Storage,
    public plt: Platform,
    private ngZone: NgZone,
    private constURL: URLs,
    private toastController: ToastController,
    private loadingCtrl: LoadingController,
    public events: Events
  ) {
    this.hideplayback = false;
    this.target = 0;
    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
    if (this.route.snapshot.data['hist']) {
      this.paramData = this.route.snapshot.data['hist'];
      // console.log("parammap history: ", this.paramData)
    }
  }

  inter(key) {

  }

  ngOnInit() {
    // console.log("parammap history: ", this.paramData)
  }

  ngOnDestroy() {
    localStorage.removeItem("markerTarget");
  }

  ionViewWillEnter() {
    localStorage.removeItem("markerTarget");
    this.plt.ready();
    this.ngZone.run(() => this.initMap());
  }

  ionViewDidEnter() {
    // localStorage.removeItem("markerTarget");
    // this.plt.ready();
    // this.ngZone.run(() => this.initMap());
    this.ionicStorage.get('token').then((val) => {
      console.log('Your age is', val);
      console.log("parammap history inside ionview: ", this.paramData)
      this.userdetails = val;
      this.trackerId = this.paramData.Device_ID;
      this.trackerType = this.paramData.iconType;
      this.DeviceId = this.paramData._id;
      this.trackerName = this.paramData.Device_Name;
      this.maphistory();
    });
  }

  initMap() {
    if(this.map === undefined) {
      this.map = GoogleMaps.create(this.element.nativeElement);
      this.map.one(GoogleMapsEvent.MAP_READY).then((data: any) => { });
    }
  }

  maphistory() {
    if (new Date(this.datetimeEnd).toISOString() >= new Date(this.datetimeStart).toISOString()) {

    } else {
      this.toastController.create({
        message: 'To time always greater than From Time',
        duration: 1500,
        position: 'bottom'
      }).then((toastEl => {
        toastEl.present();
      }));
      return false;
    }
    var burl = this.constURL.mainUrl + 'gps/getDistanceSpeed?imei=' + this.trackerId + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
    this.loadingCtrl.create({
      keyboardClose: true, message: 'Loging in...'
    }).then(loadingEl => {
      loadingEl.present();
      this.appService.getService(burl)
        .subscribe(data => {
          loadingEl.dismiss();
          var respData = JSON.parse(JSON.stringify(data));
          if (respData["Average Speed"] == 'NaN') {
            respData.AverageSpeed = 0;
          } else {
            respData.AverageSpeed = respData["Average Speed"];
          }
          respData.IdleTime = respData["Idle Time"];
          this.data2 = respData;
          this.hideplayback = true;
          this.callgpsFunc();
        });
    });
  }

  callgpsFunc() {
    var brurl = this.constURL.mainUrl + 'gps/v2?id=' + this.trackerId + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
    let that = this;
    this.loadingCtrl.create({
      keyboardClose: true, message: 'Loging in...'
    }).then(loadingEl => {
      loadingEl.present();
      this.appService.getService(brurl)
        .subscribe(data => {
          loadingEl.dismiss();
          var respData = JSON.parse(JSON.stringify(data));
          if (respData.length > 1) {
            that.gps(respData.reverse());
          } else {
            this.toastController.create({
              message: 'No Data found for selected vehicle..',
              duration: 1500,
              position: 'middle'
            }).then((toastEl) => {
              toastEl.present();
            })
          }

        });
    });
  }

  gps(data3) {

    let that = this;
    // that.latlongObjArr = data3;
    that.dataArrayCoords = [];
    for (var i = 0; i < data3.length; i++) {
      if (data3[i].lat && data3[i].lng) {
        var arr = [];
        var startdatetime = new Date(data3[i].insertionTime);
        arr.push(data3[i].lat);
        arr.push(data3[i].lng);
        arr.push({ "time": startdatetime.toLocaleString() });
        arr.push({ "speed": data3[i].speed });
        that.dataArrayCoords.push(arr);
      }
    }

    that.mapData = [];
    that.mapData = data3.map(function (d) {
      return { lat: d.lat, lng: d.lng };
    })
    that.mapData.reverse();

    // if (that.map != undefined) {
    //   that.map.remove();
    // }

    let bounds = new LatLngBounds(that.mapData);

    // let mapOptions = {
    //   gestures: {
    //     rotate: false,
    //     tilt: false
    //   },
    //   // mapType: that.mapKey
    // }

    // that.map = GoogleMaps.create(this.element.nativeElement, mapOptions);
    that.map.moveCamera({
      target: bounds
    })
    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(
      (data) => {
        console.log('Click MAP');

        // that.drawerHidden1 = true;
      }
    );
    // if (that.locations[0] != undefined) {              // check if there is stoppages or not
    //   for (var k = 0; k < that.locations[0].length; k++) {
    //     that.setStoppages(that.locations[0][k]);
    //   }
    // }

    that.map.addMarker({
      title: 'D',
      position: that.mapData[0],
      icon: 'red',
      styles: {
        'text-align': 'center',
        'font-style': 'italic',
        'font-weight': 'bold',
        'color': 'red'
      },
    }).then((marker: Marker) => {
      marker.showInfoWindow();
      that.map.addMarker({
        title: 'S',
        position: that.mapData[that.mapData.length - 1],
        icon: 'green',
        styles: {
          'text-align': 'center',
          'font-style': 'italic',
          'font-weight': 'bold',
          'color': 'green'
        },
      }).then((marker: Marker) => {
        marker.showInfoWindow();
      });
    });

    that.map.addPolyline({
      points: that.mapData,
      color: '#635400',
      width: 3,
      geodesic: true
    })
  }

  Playback() {
    let that = this;
    // that.showZoom = true;
    if (localStorage.getItem("markerTarget") != null) {
      that.target = JSON.parse(localStorage.getItem("markerTarget"));
    }
    that.playing = !that.playing; // This would alternate the state each time

    var coord = that.dataArrayCoords[that.target];
    that.coordreplaydata = coord;
    var lat = coord[0];
    var lng = coord[1];

    that.startPos = [lat, lng];
    that.speed = 200; // km/h

    if (that.playing) {
      that.map.setCameraTarget({ lat: lat, lng: lng });
      if (that.allData.mark == undefined) {
        var icicon;
        if (that.plt.is('ios')) {
          icicon = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
        } else if (that.plt.is('android')) {
          icicon = './assets/imgs/vehicles/running' + that.trackerType + '.png';
        }
        that.map.addMarker({
          icon: icicon,
          styles: {
            'text-align': 'center',
            'font-style': 'italic',
            'font-weight': 'bold',
            'color': 'green'
          },
          position: new LatLng(that.startPos[0], that.startPos[1]),
        }).then((marker: Marker) => {
          that.allData.mark = marker;
          that.liveTrack(that.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
        });
      } else {
        that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
        that.liveTrack(that.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
      }
    } else {
      that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
    }
  }

  liveTrack(map, mark, coords, target, startPos, speed, delay) {
    let that = this;
    that.events.subscribe("SpeedValue:Updated", (sdata) => {
      speed = sdata;
    })
    var target = target;
    if (!startPos.length)
      coords.push([startPos[0], startPos[1]]);

    function _gotoPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000;
      if (coords[target] == undefined)
        return;
      var dest = new LatLng(coords[target][0], coords[target][1]);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); //in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target][0] - lat) / numStep;
      var deltaLng = (coords[target][1] - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
      }

      function _moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          mark.setPosition(new LatLng(lat, lng));
          map.setCameraTarget(new LatLng(lat, lng))
          setTimeout(_moveMarker, delay);
        } else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          mark.setPosition(dest);
          map.setCameraTarget(dest);
          target++;
          setTimeout(_gotoPoint, delay);
        }
      }
      a++
      if (a > coords.length) {

      } else {
        that.speedMarker = coords[target][3].speed;
        that.updatetimedate = coords[target][2].time;

        if (that.playing) {
          _moveMarker();
          target = target;
          localStorage.setItem("markerTarget", target);

        } else { }
        // km_h = km_h;
      }
    }
    var a = 0;
    _gotoPoint();
  }
  //  loadMap() {
  // //   Environment.setEnv({
  // //     API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyDh51bMF0nRsnIoqNRq3tO_ruH5KNHoGiM',
  // //     API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyDh51bMF0nRsnIoqNRq3tO_ruH5KNHoGiM'
  // //   });
  //    this.map = GoogleMaps.create('map_canvas');
  //  }
}
