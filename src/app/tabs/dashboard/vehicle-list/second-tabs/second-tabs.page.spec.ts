import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondTabsPage } from './second-tabs.page';

describe('SecondTabsPage', () => {
  let component: SecondTabsPage;
  let fixture: ComponentFixture<SecondTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondTabsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
