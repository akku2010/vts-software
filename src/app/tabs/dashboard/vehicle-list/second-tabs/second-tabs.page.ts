import { Component, OnInit } from '@angular/core';
// import { Events } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-second-tabs',
  templateUrl: './second-tabs.page.html',
  styleUrls: ['./second-tabs.page.scss'],
})
export class SecondTabsPage implements OnInit {
  tabPath: any;
  clicked: boolean = false;

  constructor(
    // public events: Events,
    public dataService: DataService,
    public ionicStorage: Storage,
    private router: Router
  ) { 
  }

  history() {
    this.clicked = true;
    console.log("clicked here")
    this.ionicStorage.get('vehicleToken').then((val) => {
      console.log("history data: "+ val);
      this.dataService.setData(43, JSON.parse(JSON.stringify(val)));
      this.router.navigateByUrl('/maintabs/tabs/vehicle-list/second-tabs/second-main-tabs/history/43');
    });
  }

  ngOnInit() {
  }

  // clickTab(event: Event, tab: string) {
  //   event.stopImmediatePropagation();
  //   this.router.navigate([`${this.tabPath}${tab}`]);
  // }

}
