import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { AppService } from 'src/app/app.service';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  GoogleMapsMapTypeId,
  LatLng,
  Spherical,
  CameraPosition
} from '@ionic-native/google-maps/ngx';
import * as _ from "lodash";

import { DrawerState } from 'ion-bottom-drawer';
import { URLs } from 'src/app/app.model';
import { Storage } from '@ionic/storage';
import { Platform, LoadingController, Events, AlertController, ToastController } from '@ionic/angular';
import { mapStyle } from './map-style.model';

@Component({
  selector: 'app-live-track',
  templateUrl: './live-track.page.html',
  styleUrls: ['./live-track.page.scss'],
})
export class LiveTrackPage implements OnInit {
  data: any;
  @ViewChild('map123', {
    static: true
  }) element: ElementRef;
  map123: GoogleMap;
  afterClick: boolean = false;
  afterMapTypeClick: boolean = false;
  shouldBounce = true;
  dockedHeight = 52;
  distanceTop = 150;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 52;
  userdetails: any;
  socketChnl: any = [];
  allData: any = {};
  socketSwitch: {};
  impkey: any;
  deviceDeatils: any;
  loading: any;

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  clicked: boolean = false;
  messages: string;
  deviceConfigStatus: any;
  intervalID: any;
  device_type: any;

  constructor(
    private route: ActivatedRoute,
    private toastController: ToastController,
    // private router: Router,
    public googleMaps: GoogleMaps,
    public appService: AppService,
    public elementRef: ElementRef,
    private ionicStorage: Storage,
    public plt: Platform,
    private constURL: URLs,
    private socket: Socket,
    private loadingCtrl: LoadingController,
    private ngZone: NgZone,
    public events: Events,
    private alertController: AlertController
  ) {
    if (this.route.snapshot.data['special']) {
      this.data = this.route.snapshot.data['special'];
      this.device_type = this.data.device_model.device_type;
      console.log("parammap: ", this.data)
    }
  }

  ngOnInit() {
    this.allData = {};
    this.socketChnl = [];
    this.socketSwitch = {};
    // console.log("Hello Deepa, I'm here!!")
    // this.socket.connect();
    // this.plt.ready();
    // this.ngZone.run(() => this.initMap());
    // this.ionicStorage.get('token').then((val) => {
    //   console.log('Your age is', val);
    //   this.userdetails = val;
    //   // this.events.publish('data:live-track', this.data);
    //   this.live(this.data);
    // });
  }
  ngOnDestroy() {
    // let that = this;
    // for (var i = 0; i < that.socketChnl.length; i++)
    //   that.socket.removeAllListeners(that.socketChnl[i]);
    // that.socket.disconnect();
  }

  ionViewWillEnter() {
    console.log("Hello Anjali, I'm here!!")
    // this.socket.disconnect(); 
    this.socket.connect();
    this.plt.ready();
    this.ngZone.run(() => this.initMap());
    // this.socketInit(this.data);
  }

  ionViewWillLeave() {
    this.socket.disconnect();
  }

  ionViewDidEnter() {
    console.log("Hello Deepa, I'm here!!")
    // this.socket.connect();
    // this.plt.ready();
    // this.ngZone.run(() => this.initMap());
    this.ionicStorage.get('token').then((val) => {
      console.log('Your age is', val);
      this.userdetails = val;
      this.live(this.data);
    });
  }

  initMap() {
    // debugger
    let style = [];
    //Change Style to night between 7pm to 5am
    if (this.isNight()) {
      style = mapStyle
    }
    let mapOptions = {
      backgroundColor: 'white',
      controls: {
        compass: true,
        zoom: false,
      },
      gestures: {
        rotate: false,
        tilt: false
      },
      // mapType: that.mapKey,
      styles: style
    }
    debugger
    if (this.map123 === undefined) {
      console.log(this.element.nativeElement)
      this.map123 = GoogleMaps.create(this.element.nativeElement, mapOptions);
      this.map123.one(GoogleMapsEvent.MAP_READY).then((data: any) => {
        // this.onButtonClick();
      })
    }
  }

  onTraffic() {
    this.afterClick = !this.afterClick;
    if (this.afterClick) {
      this.map123.setTrafficEnabled(true);
    } else {
      this.map123.setTrafficEnabled(false);
    }
  }

  onMapType() {
    this.afterMapTypeClick = !this.afterMapTypeClick;
    if (this.afterMapTypeClick) {
      this.map123.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
    } else {
      this.map123.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
    }
  }

  async onButtonClick() {
    this.map123.clear();

    this.loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    await this.loading.present();

    // Get the location of you
    this.map123.getMyLocation().then((location: MyLocation) => {
      this.loading.dismiss();
      console.log(JSON.stringify(location, null, 2));

      // Move the map camera to the location with animation
      // this.map.animateCamera({
      //   target: location.latLng,
      //   zoom: 17,
      //   tilt: 30
      // });

      // add a marker
      let marker: Marker = this.map123.addMarkerSync({
        title: '@ionic-native/google-maps plugin!',
        snippet: 'This plugin is awesome!',
        position: location.latLng,
        animation: GoogleMapsAnimation.BOUNCE
      });

      // show the infoWindow
      marker.showInfoWindow();

      // If clicked it, display the alert
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        // this.showToast('clicked!');
      });
    })
      .catch(err => {
        this.loading.dismiss();
        // this.showToast(err.error_message);
      });
  }


  live(data) {
    let that = this;
    // if (that.map123 != undefined) {
    //   that.map123.remove();
    // }
    for (var i = 0; i < that.socketChnl.length; i++)
      that.socket.removeAllListeners(that.socketChnl[i]);

    // that.allData = {};
    // that.socketChnl = [];
    // that.socketSwitch = {};
    // let style = [];

    //Change Style to night between 7pm to 5am
    // if (this.isNight()) {
    //   style = mapStyle
    // }

    if (data) {
      if (data.last_location) {
        if (that.plt.is('android')) {
          // let mapOptions = {
          //   backgroundColor: 'white',
          //   controls: {
          //     compass: true,
          //     zoom: false,
          //   },
          //   gestures: {
          //     rotate: false,
          //     tilt: false
          //   },
          //   // mapType: that.mapKey,
          //   styles: style
          // }
          // that.map123 = GoogleMaps.create(this.element.nativeElement, mapOptions);

          // that.map123.animateCamera({
          //   // target: targets.length === 1 ? targets[0] : targets,
          //   target: { lat: 20.5937, lng: 78.9629 },
          //   zoom: 18,
          //   duration: 3000,
          //   padding: 0  // default = 20px
          // });

          that.map123.moveCamera({
            // target: targets.length === 1 ? targets[0] : targets,
            // target: { lat: 20.5937, lng: 78.9629 },
            target: { lat: data.last_location['lat'], lng: data.last_location['long'] },
            zoom: 18,
            duration: 3000,
            padding: 0  // default = 20px
          });

          // that.map123.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });

        } else {
          // if (that.plt.is('ios')) {
          //   let mapOptions = {
          //     backgroundColor: 'white',
          //     controls: {
          //       compass: true,
          //       zoom: false,
          //     },
          //     gestures: {
          //       rotate: false,
          //       tilt: false
          //     },
          //     camera: {
          //       target: { lat: 20.5937, lng: 78.9629 },
          //       zoom: 18,
          //     },
          //     // mapType: that.mapKey,
          //     styles: style
          //   }
          //   let map = GoogleMaps.create(this.element.nativeElement, mapOptions);
          //   // Environment.setBackgroundColor("black");
          //   that.map123 = map;
          // }
        }

        that.socketInit(data);
      } else {
        // that.map = that.initMap();
        // that.socketInit(data);
      }
    }
  }

  onImmo() {
    debugger
    let that = this;
    let d = that.data;
    if (d.last_ACC != null || d.last_ACC != undefined) {

      if (that.clicked) {
        this.alertController.create({
          message: "Process is already going on. Please wait till complete.",
          buttons: ['Okay']
        }).then(alertEl => {
          alertEl.present();
        })
      } else {
        this.messages = undefined;
        // this.dataEngine = d;
        // this.apiCall.startLoading().present();
        this.loadingCtrl.create({
          message: 'Please wait...'
        }).then(loadEl => {
          loadEl.present();
          this.subFunc(d, loadEl);
        })

      }
    }

  }

  subFunc(d, loadEl) {
    let that = this;
    var baseURLp;
    if (d.device_model.device_type === undefined) {
      baseURLp = this.constURL.mainUrl + 'deviceModel/getDevModelByName?type=' + this.device_type;

    } else {
      baseURLp = this.constURL.mainUrl + 'deviceModel/getDevModelByName?type=' + d.device_model.device_type;
    }

    this.appService.getService(baseURLp)
      .subscribe(data => {
        loadEl.dismiss();
        debugger;
        console.log("immo data: ", data)
        this.deviceConfigStatus = data;
        let immobType = data[0].imobliser_type;
        if (d.ignitionLock == '1') {
          this.messages = 'Do you want to unlock the engine?';
        } else {
          if (d.ignitionLock == '0') {
            this.messages = 'Do you want to lock the engine?'
          }
        }
        this.alertController.create({
          message: this.messages,
          buttons: [{
            text: 'YES',
            handler: () => {
              if (immobType == 0 || immobType == undefined) {
                that.clicked = true;
                var devicedetail = {
                  "_id": d._id,
                  "engine_status": !d.engine_status
                }
                const bUrl = this.constURL.mainUrl + 'devices/deviceupdate';
                // this.apiCall.startLoading().present();
                this.appService.getServiceWithParams(bUrl, devicedetail)
                  .subscribe(response => {
                    // this.apiCall.stopLoading();
                    // this.editdata = response;
                    let res = JSON.parse(JSON.stringify(response));
                    this.toastController.create({
                      message: res.message,
                      duration: 2000,
                      position: 'top'
                    }).then(toastEl => {
                      toastEl.present();
                    })
                    // this.responseMessage = "Edit successfully";
                    // this.getdevices();

                    var msg;
                    if (!d.engine_status) {
                      msg = this.deviceConfigStatus[0].resume_command;
                    }
                    else {
                      msg = this.deviceConfigStatus[0].immoblizer_command;
                    }

                    // this.sms.send(d.sim_number, msg);
                    // const toast1 = this.toastCtrl.create({
                    //   message: this.translate.instant('SMS sent successfully'),
                    //   duration: 2000,
                    //   position: 'bottom'
                    // });
                    // toast1.present();
                    that.clicked = false;
                  },
                    error => {
                      that.clicked = false;
                      // this.apiCall.stopLoading();
                      console.log(error);
                    });
              } else {
                console.log("Call server code here!!")
                that.serverLevelOnOff(d);
              }
            }
          },
          {
            text: 'NO'
          }]
        }).then(alertEl => {
          alertEl.present();
        });
      },
        error => {
          loadEl.dismiss();
          // this.apiCall.stopLoading();
          console.log("some error: ", error._body.message);
        });
  }

  serverLevelOnOff(d) {
    let that = this;
    that.clicked = true;
    var data: any = {};
    if (d.device_model.device_type === undefined) {
      data = {
        "imei": d.Device_ID,
        "_id": d._id,
        "engine_status": d.ignitionLock,
        "protocol_type": this.device_type
      }

    } else {
      data = {
        "imei": d.Device_ID,
        "_id": d._id,
        "engine_status": d.ignitionLock,
        "protocol_type": d.device_model.device_type
      }
    }

    let bURL = this.constURL.mainUrl + 'devices/addCommandQueue';
    // this.apiCall.startLoading().present();
    this.appService.getServiceWithParams(bURL, data)
      .subscribe(resp => {
        // this.apiCall.stopLoading();
        console.log("ignition on off=> ", resp)
        let respMsg = JSON.parse(JSON.stringify(resp));
        const urlTel = this.constURL.mainUrl + 'trackRouteMap/getRideStatusApp?_id=' + respMsg._id;
        // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
        this.intervalID = setInterval(() => {
          this.appService.getService(urlTel)
            .subscribe(data => {
              console.log("interval=> " + data)
              let commandStatus = JSON.parse(JSON.stringify(data)).status;

              if (commandStatus == 'SUCCESS') {
                clearInterval(this.intervalID);
                that.clicked = false;
                this.callObjFunc(d);
                // this.apiCall.stopLoadingnw();
                this.toastController.create({
                  message: 'Process has been completed successfully...!!',
                  duration: 1500,
                  position: 'bottom'
                }).then((toastEl => {
                  toastEl.present();
                }));
              }
            })
        }, 5000);
      },
        err => {
          // this.apiCall.stopLoading();
          console.log("error in onoff=>", err);
          that.clicked = false;
        });
  }

  callObjFunc(d) {
    let that = this;
    var _bUrl = this.constURL.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
    this.loadingCtrl.create({
      message: 'Plaese wait...'
    }).then(loadEl => {
      loadEl.present();
      this.appService.getService(_bUrl)
        .subscribe(resp => {
          loadEl.dismiss();
          console.log("updated device object=> " + resp);
          if (!resp) {
            return;
          } else {
            that.data = resp;
          }
        })
    });
    // this.apiCall.startLoading().present();

  }


  isNight() {
    //Returns true if the time is between
    //7pm to 5am
    let time = new Date().getHours();
    return (time > 5 && time < 19) ? false : true;
  }



  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';

  carIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };

  icons = {
    "car": this.carIcon,
    "bike": this.carIcon,
    "truck": this.carIcon,
    "bus": this.carIcon,
    "user": this.carIcon,
    "jcb": this.carIcon,
    "tractor": this.carIcon
  }

  socketInit(pdata, center = false) {

    this.socket.emit('acc', pdata.Device_ID);
    this.socketChnl.push(pdata.Device_ID + 'acc');
    let that = this;
    this.socket.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
      if (d4 != undefined)
        console.log("ping data: => ", d4);
      (function (data) {
        if (data == undefined) {
          return;
        }
        // localStorage.setItem("pdata", JSON.stringify(data))

        if (data._id != undefined && data.last_location != undefined) {
          var key = data._id;
          that.impkey = data._id;
          that.deviceDeatils = data;
          let ic = _.cloneDeep(that.icons[data.iconType]);
          if (!ic) {
            return;
          }
          ic.path = null;

          if (data.status.toLowerCase() === 'running' && data.last_speed > 0) {
            if (that.plt.is('ios')) {
              ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
            } else if (that.plt.is('android')) {
              ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
            }
          } else {
            if (data.status.toLowerCase() === 'running' && data.last_speed === 0) {
              if (that.plt.is('ios')) {
                ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
              } else if (that.plt.is('android')) {
                ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
              }
            } else {
              if (that.plt.is('ios')) {
                ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
              } else if (that.plt.is('android')) {
                ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
              }
            }
          }
          if (that.allData[key]) {
            that.socketSwitch[key] = data;
            console.log("check mark is avalible or not? ", that.allData[key].mark)
            if (that.allData[key].mark !== undefined) {
              that.allData[key].mark.setIcon(ic);
              that.allData[key].mark.setPosition(that.allData[key].poly[1]);
              var temp = _.cloneDeep(that.allData[key].poly[1]);
              that.allData[key].poly[0] = _.cloneDeep(temp);
              that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

              var speed = data.status == "RUNNING" ? data.last_speed : 0;
              // that.getAddress(data.last_location);
              // debugger
              that.liveTrack(that.map123, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
            } else {
              return;
            }
          }
          else {
            that.allData[key] = {};
            that.socketSwitch[key] = data;
            that.allData[key].poly = [];
            if (data.sec_last_location) {
              that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
            } else {
              that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
            }
            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
            if (data.last_location != undefined) {

              that.map123.addMarker({
                title: data.Device_Name,
                position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                icon: ic.url,
              }).then((marker: Marker) => {

                that.allData[key].mark = marker;
                marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                  .subscribe(e => {
                    // that.liveVehicleName = data.Device_Name;
                    // that.showActionSheet = true;
                    // that.showaddpoibtn = true;

                    // that.getAddressTitle(marker);

                  });
                // that.getAddress(data.last_location);
                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                that.liveTrack(that.map123, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
              })
            }
          }
        }
      })(d4)
    })
  }

  liveTrack(map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);
    if (center) {
      // map.setCameraTarget(coords[0]);
      map.moveCamera(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        // mark.setIcon(icons);
      }
      function _moveMarker() {
        // debugger
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        // var cameraPosition: CameraPosition<LatLng> = map.getCameraPosition();

        if (i < distance) {
          debugger
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
            // let cameraPosition: CameraPosition<LatLng> = map.getCameraPosition();
            // cameraPosition.bearing = 90; // heading of the map
            // map.setCameraBearing(head);
            // cameraPosition.bearing = head; // heading of the map
            // cameraPosition.target = new LatLng(lat, lng);
            // map.animateCamera(cameraPosition);
          }
          if (that.mapTrail) {
            that.showTrail(lat, lng, map);
          } else {
            if (that.allData.polylineID) {
              that.tempArray = [];
              that.allData.polylineID.remove();
            }
          }
          // map.moveCamera(new LatLng(lat, lng));
          // map.moveCamera({
          //   target: {
          //     lat: lat,
          //     lng: lng
          //   },
          //   zoom: 18
          // });

          map.setCameraTarget(new LatLng(lat, lng));
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
            // let cameraPosition: CameraPosition<LatLng> = map.getCameraPosition();
            // cameraPosition.bearing = 90; // heading of the map
            // map.setCameraBearing(head);
            // cameraPosition.bearing = head; // heading of the map
            // cameraPosition.target = dest;
            // map.animateCamera(cameraPosition);
          }
          if (that.mapTrail) {
            that.showTrail(dest.lat, dest.lng, map);
          }
          else {
            if (that.allData.polylineID) {
              that.tempArray = [];
              that.allData.polylineID.remove();
            }
          }
          // let cameraPosition: CameraPosition<LatLng> = this.map.getCameraPosition();

          // cameraPosition.bearing = 90; // heading of the map

          // this.map.animateCamera(cameraPosition);
          map.setCameraTarget(dest);
          // map.moveCamera(dest);
          // map.moveCamera({
          //   target: {
          //     lat: dest.lat,
          //     lng: dest.lng
          //   },
          //   zoom: 18
          // });
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }
}
