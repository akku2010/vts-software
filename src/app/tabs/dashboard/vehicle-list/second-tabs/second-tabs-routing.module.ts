import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SecondTabsPage } from './second-tabs.page';
import { DataResolverService } from 'src/app/resolver/data-resolver.service';

const routes: Routes = [
    {
        path: 'second-main-tabs',
        component: SecondTabsPage,
        children: [
            {
                path: 'live-track',
                children: [
                    {
                        path: '',
                        loadChildren: './live-track/live-track.module#LiveTrackPageModule'
                    },
                    {
                        path: ':id',
                        resolve: {
                            special: DataResolverService
                        },
                        loadChildren: './live-track/live-track.module#LiveTrackPageModule'
                    },
                ]
            },
            {
                path: 'history',
                children: [
                    {
                        path: ':id',
                        resolve: {
                            hist: DataResolverService
                        },
                        loadChildren: './history/history.module#HistoryPageModule'
                    },
                ]
            },
            {
                path: 'analytics',
                loadChildren: './analytics/analytics.module#AnalyticsPageModule'
            },
            {
                path: 'service',
                loadChildren: './service/service.module#ServicePageModule'
            },
            {
                path: 'more',
                loadChildren: './more/more.module#MorePageModule'
            },
            {
                path: '',
                redirectTo: '/maintabs/tabs/dashboard',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/maintabs/tabs/dashboard',
        pathMatch: 'full'
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)]
})
export class SecondTabsRoutingModule {

}