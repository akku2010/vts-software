import { Component, OnInit } from "@angular/core";
import { AppService } from "src/app/app.service";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-alerts",
  templateUrl: "./alerts.page.html",
  styleUrls: ["./alerts.page.scss"]
})
export class AlertsPage implements OnInit {
  userData: any;
  limit: number = 50;
  pageNo: number = 1;
  notification: any;
  constructor(private appService: AppService, private ionicStorage: Storage) {}
  ionViewWillEnter() {
    this.ionicStorage.get("token").then(val => {
      this.userData = val;
      this.getNotifications();
    });
  }
  ngOnInit() {
    // this.ionicStorage.get("token").then(val => {
    //   this.userData = val;
    //   this.getNotifications();
    // });
  }

  getNotifications() {
    var _bUrl =
      'https://www.oneqlik.in/notifs/' +
      'getNotifiLimit?user=' +
      this.userData._id +
      '&pageNo=' +
      this.pageNo +
      '&size=' +
      this.limit;
    this.appService.getService(_bUrl).subscribe(resp => {
      console.log(resp);
      this.notification = JSON.parse(JSON.stringify(resp));
      // console.log(this.notification);
    });
  }
}
