import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DashboardPage } from './dashboard.page';
// import { LiveComponent } from './live/live.component';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';
// import { LiveComponent } from './live/live.component';
// import { GoogleMaps } from '@ionic-native/google-maps/ngx';
const routes: Routes = [
  {
    path: '',
    component: DashboardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    IonBottomDrawerModule
  ],
  declarations: [DashboardPage],
  // entryComponents: [LiveComponent],
  // providers: [GoogleMaps]
})
export class DashboardPageModule { }
