import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { LiveComponent } from './live.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
// import { GoogleMaps } from '@ionic-native/google-maps/ngx';
const routes: Routes = [
    {
        path: '',
        component: LiveComponent
    }
];
@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [LiveComponent],
    // providers: [GoogleMaps]
})


export class LiveComponentModule {}