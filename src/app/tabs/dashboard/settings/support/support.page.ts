import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-support",
  templateUrl: "./support.page.html",
  styleUrls: ["./support.page.scss"]
})
export class SupportPage implements OnInit {
  userdetails: any = {};
  data: any;
  parammap: any;
  constructor(public ionicStorage: Storage, private route: ActivatedRoute) {
    // let that = this;
    // this.route.queryParams.subscribe(params => {
    //   if (params && params.special) {
    //     this.data = JSON.parse(params.special);
    //   }
    // });
    // console.log('parammap: ', that.data);
    this.userdetails = {

    };
    const that = this;
    this.ionicStorage.get('token').then(val => {
      console.log('Your data is', val);
      that.userdetails = val;
      console.log(that.userdetails);
      console.log(that.userdetails.email);
      // that.ionicStorage.forEach((val) => {
      //   that.userdetails = val;
      //   });
    });
  }

  ngOnInit() {}
}
