import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: "app-settings",
  templateUrl: "./settings.page.html",
  styleUrls: ["./settings.page.scss"]
})
export class SettingsPage implements OnInit {
  userdetails: any ={};

  constructor(
    private router: Router,
    private ionicStorage: Storage,
    private authService: AuthenticationService,
    private auth: AuthService
  ) { }

  ionViewWillEnter() {
    this.ionicStorage.get("token").then(val => {
      console.log("Your data is", val);
      this.userdetails = val;
    });
  }

  ngOnInit() {
    // this.ionicStorage.get("token").then(val => {
    //   console.log("Your data is", val);
    //   this.userdetails = val;
    // });
  }

  onLogout() {
    // this.ionicStorage.remove('token').then(val => {
    //   this.ionicStorage.remove('vehicleToken');
    //   this.router.navigateByUrl("/login");
    // })

    this.authService.logout();
    // this.auth.logout();
  }

  onClick() {
    this.router.navigateByUrl("/maintabs/tabs/settings/configure");
  }
  cpassword() {
    this.router.navigateByUrl("/maintabs/tabs/settings/change-password");
  }
  support() {
    this.router.navigateByUrl('/maintabs/tabs/settings/support');
  }
}
