import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authState = new BehaviorSubject(false);
  constructor(
    private storage: Storage,
    private platform: Platform,
    public toastController: ToastController,
    private router: Router
  ) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }

  ifLoggedIn() {
    this.storage.get('token').then((response) => {
      if (response) {
        this.authState.next(true);
      }
    });
  }


  login() {
    this.authState.next(true);
  }

  logout() {
    this.storage.remove('token').then(val => {
      this.storage.remove('vehicleToken');
      this.router.navigate(['login']);
      this.authState.next(false);
    })
    // this.storage.remove('token').then(() => {
    //   this.router.navigate(['login']);
    //   this.authState.next(false);
    // });
  }

  isAuthenticated() {
    return this.authState.value;
  }

}
